FROM ubuntu:14.04

COPY .ansible.cfg /root

RUN apt-get update && apt-get install -y software-properties-common && apt-add-repository -y ppa:ansible/ansible && apt-get update && apt-get -y install ansible